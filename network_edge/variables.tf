variable "equinix_client_id" {

}

variable "equinix_client_secret" {

}

variable "aws_account_id" {
}

variable "aws_access_key" {

}

variable "aws_secret_key" {

}

variable "region" {
    type = string
    default = "us-east-2"
}

variable "name" {
    type = string
    default = "test-cvim"
}

variable "throughput" {
    type = number
    default = 500
}

variable "throughput_unit" {
    type = string
    default = "Mbps"
}

variable "metro_code" {

}

variable "type_code" {

}

variable "term_length" {
    type = string
    default = "1"
}

variable "package_code" {
    type = string
    default = "IPBASE"
}

variable "notifications" {
    type = list
    default = ["user@equinix.com"]
}

variable "hostname" {
    type = string
    default = "test-cvim"
}

variable "ssh_users" {

}

variable "acls" {
    type = list
    default = []
}

variable "account_number" {

}

variable "sw_version" {


variable "core_count" {

}

variable "speed" {

}

variable "connection_name" {

}
variable "equinix_port_name" {

}

variable "aws_sp" {

}

variable "local_ip_address" {

}

variable "local_asn" {


variable "remote_ip_address" {

}

variable "remote_asn" {

}

variable "bgp_auth_key" {

}


