provider "equinix" {
  client_id     = var.equinix_client_id
  client_secret = var.equinix_client_secret
  request_timeout = 10
}

data "equinix_ecx_l2_sellerprofile" "aws" {
  name = "AWS Direct Connect"
}

resource "equinix_network_device" "test-edge" {
  name               = var.name
  throughput         = var.throughput
  throughput_unit    = var.throughput_unit
  metro_code         = var.metro_code
  type_code          = var.type_code
  package_code       = var.package_code
  notifications      = var.notifications
  hostname           = var.hostname
  acls               = var.acls
  term_length        = 1
  account_number     = var.account_number
  version            = var.sw_version
  core_count         = var.core_count
}

resource "equinix_ecx_l2_connection" "aws-conn-1" {
  name               = var.connection_name
  profile_uuid       = data.equinix_ecx_l2_sellerprofile.aws.id
  speed              = var.speed
  speed_unit         = "MB"
  notifications      = var.notifications
  device_uuid         = equinix_network_device.test-edge.id
  seller_region      = "us-east-2"
  seller_metro_code  = var.metro_code
  authorization_key  = var.aws_account_id
}

resource "equinix_ecx_l2_connection_accepter" "accepter" {
  connection_id = equinix_ecx_l2_connection.aws-conn-1.id
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
}

resource "equinix_network_bgp" "test" {
  connection_id      = equinix_ecx_l2_connection.aws-conn-1.id
  local_ip_address   = var.local_ip_address
  local_asn          = var.local_asn
  remote_ip_address  = var.remote_ip_address
  remote_asn         = var.remote_asn
  authentication_key = var.bgp_auth_key
}