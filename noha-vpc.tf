# Terraform Script to build AWS infrastructure needed for CVIM Upgrades (non-HA)
# Run the following commands before terraform apply
# export AWS_ACCESS_KEY_ID="anaccesskey"
# export AWS_SECRET_ACCESS_KEY="asecretkey"
# export AWS_DEFAULT_REGION="aregion"
# Note: if you add a space before the export command it will not be saved to history file on disk

# Build VPC-1 Non-HA Infrastructure

provider "aws" {
  region = var.region
}

resource "aws_ami_copy" "ami-prtg" {
  name              = "windows_server_prtg"
  description       = "PRTG Network Monitor"
  source_ami_id     = "ami-094c20ad130e59bab"
  source_ami_region = "eu-central-1"

  tags = {
    Name = "AMI-PRTG"
  }
}

resource "aws_vpc" "vpc-1" {
  cidr_block            = "10.100.0.0/24"
  instance_tenancy      = "default"
  enable_dns_hostnames  = true

  tags = {
    Name = "VPC-1"
  }
}

resource "aws_subnet" "subnet-1" {
  vpc_id     = aws_vpc.vpc-1.id
  cidr_block = "10.100.0.0/27"

  tags = {
    Name = "Subnet-1"
  }
}

resource "aws_route_table" "route-1" {
  vpc_id = aws_vpc.vpc-1.id
  
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw-1.id
  }

  tags = {
    Name = "Route-1"
  }
}

resource "aws_route_table_association" "route-assoc-1" {
  subnet_id      = aws_subnet.subnet-1.id
  route_table_id = aws_route_table.route-1.id
}

resource "aws_internet_gateway" "igw-1" {
  vpc_id = aws_vpc.vpc-1.id
  tags = {
    Name = "IGW-1"
  }
}

resource "aws_vpn_gateway" "vgw-1" {
  vpc_id            = aws_vpc.vpc-1.id
  amazon_side_asn   = "65100"

  tags = {
    Name = "VGW-1"
  }
}

resource "aws_vpn_gateway_route_propagation" "route-prop-1" {
  vpn_gateway_id = aws_vpn_gateway.vgw-1.id
  route_table_id = aws_route_table.route-1.id
}

resource "aws_security_group" "sg-1" {
  name        = "Security Group-1"
  description = "Allow inbound traffic for VPC-1"
  vpc_id      = aws_vpc.vpc-1.id

  ingress {
    description = "HTTP from VPC-1"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "RDP from VPC-1"
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Any from 10/8"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["10.0.0.0/8"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Allow SG-1"
  }
}

# Data Source queries latest Windows instance and passes to next block

data "aws_ami" "prtg_image" {
  owners = ["self"]
  filter {
    name = "name"
    values = ["windows_server_prtg"]
  }
  depends_on = [aws_ami_copy.ami-prtg]
}

resource "aws_instance" "instance-1" {
    ami                      = data.aws_ami.prtg_image.id
    instance_type            = "t2.micro"
    subnet_id                = aws_subnet.subnet-1.id
    key_name                 = "cvim_upgrade_key"
    private_ip               = "10.100.0.15"
    vpc_security_group_ids   = [aws_security_group.sg-1.id]

tags = {
    Name = "Instance-1"
  }
}

resource "aws_eip" "eip-1" {
  instance = aws_instance.instance-1.id
  vpc      = true
}

# Build VPC-2 Non-HA Infrastructure

resource "aws_vpc" "vpc-2" {
  cidr_block            = "10.200.0.0/24"
  instance_tenancy      = "default"
  enable_dns_hostnames  = true

  tags = {
    Name = "VPC-2"
  }
}

resource "aws_subnet" "subnet-2" {
  vpc_id     = aws_vpc.vpc-2.id
  cidr_block = "10.200.0.0/27"

  tags = {
    Name = "Subnet-2"
  }
}

resource "aws_route_table" "route-2" {
  vpc_id = aws_vpc.vpc-2.id
  
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw-2.id
  }

  tags = {
    Name = "Route-2"
  }
}

resource "aws_route_table_association" "route-assoc-2" {
  subnet_id      = aws_subnet.subnet-2.id
  route_table_id = aws_route_table.route-2.id
}

resource "aws_internet_gateway" "igw-2" {
  vpc_id = aws_vpc.vpc-2.id
  tags = {
    Name = "IGW-2"
  }
}

resource "aws_vpn_gateway" "vgw-2" {
  vpc_id            = aws_vpc.vpc-2.id
  amazon_side_asn   = "65200"

  tags = {
    Name = "VGW-2"
  }
}

resource "aws_vpn_gateway_route_propagation" "route-prop-2" {
  vpn_gateway_id = aws_vpn_gateway.vgw-2.id
  route_table_id = aws_route_table.route-2.id
}

resource "aws_security_group" "sg-2" {
  name        = "Security Group-2"
  description = "Allow inbound traffic for VPC-2"
  vpc_id      = aws_vpc.vpc-2.id

  ingress {
    description = "SSH from VPC-2"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Any from 10/8"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Allow SG-2"
  }
}

# Data Source queries latest Amazon Linux instance and passes to next block

data "aws_ssm_parameter" "latest-instance-2" {
  name = "/aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-gp2"

 }

resource "aws_instance" "instance-2" {
    ami                      = data.aws_ssm_parameter.latest-instance-2.value
    instance_type            = "t2.micro"
    subnet_id                = aws_subnet.subnet-2.id
    key_name                 = "cvim_upgrade_key"
    private_ip               = "10.200.0.15"
    vpc_security_group_ids   = [aws_security_group.sg-2.id]

tags = {
    Name = "Instance-2"
  }
}

resource "aws_eip" "eip-2" {
  instance = aws_instance.instance-2.id
  vpc      = true
}

resource "aws_dx_private_virtual_interface" "vif-1" {
  
  connection_id = var.dx1-connection_id
  name               = "VIF-1"
  vlan               = var.dx1-vlan_id
  address_family     = "ipv4"
  bgp_asn            = 65000
  vpn_gateway_id     = aws_vpn_gateway.vgw-1.id
  bgp_auth_key       = var.bgp-auth-key
  customer_address   = "10.0.0.1/30"
  amazon_address     = "10.0.0.2/30"
}

resource "aws_dx_private_virtual_interface" "vif-2" {
  
  connection_id      = var.dx2-connection_id
  name               = "VIF-2"
  vlan               = var.dx2-vlan_id
  address_family     = "ipv4"
  bgp_asn            = 65000
  vpn_gateway_id     = aws_vpn_gateway.vgw-2.id
  bgp_auth_key       = var.bgp-auth-key
  customer_address   = "10.0.0.5/30"
  amazon_address     = "10.0.0.6/30"
}


