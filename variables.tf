# Enter the region, connection ID and VLAN ID from the AWS portal for the respective DX connection

variable "region" {
}

variable "dx1-connection_id" {
}

variable "dx1-vlan_id" {
}

variable "dx2-connection_id" {
}

variable "dx2-vlan_id" {
}

variable "bgp-auth-key" {
}